package com.cgm.academy.spitter.web;

import com.cgm.academy.spitter.data.SpittleRepository;
import com.cgm.academy.spitter.model.Spittle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;

@Controller
public class SpittleControler {
    private SpittleRepository spittleRepository;
    @Autowired
    public SpittleControler(SpittleRepository spittleRepository) {
        this.spittleRepository = spittleRepository;
    }

    @RequestMapping(value = "/spittles", method = RequestMethod.GET)
    public String spittlesSite(Model model){
        model.addAttribute(spittleRepository.findAll());
        return "spittles";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String addSpittle(String message, Model model){
        Spittle spittle = new Spittle(message, LocalDate.now());
        spittleRepository.save(spittle);
        return "redirect:/spittles";
    }
}
