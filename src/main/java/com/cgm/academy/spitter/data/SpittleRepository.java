package com.cgm.academy.spitter.data;

import com.cgm.academy.spitter.model.Spittle;
import org.springframework.data.repository.CrudRepository;

public interface SpittleRepository extends CrudRepository<Spittle,Long>{
}
