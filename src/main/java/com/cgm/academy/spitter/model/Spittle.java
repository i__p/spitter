package com.cgm.academy.spitter.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
public class Spittle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private final Long id;
    @NotNull
    private final String message;
    @NotNull
    private LocalDate date;

    public Spittle() {
        id = null;
        message = "";
        date = LocalDate.of(1970, 1, 1);
    }

    public Spittle(@NotNull String message, @NotNull LocalDate date) {
        this.id = null;
        this.message = message;
        this.date = date;
    }
}
