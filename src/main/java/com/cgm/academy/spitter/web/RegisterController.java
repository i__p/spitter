package com.cgm.academy.spitter.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class RegisterController {

    @RequestMapping(value ={"/register"}, method = RequestMethod.GET)
    public String regiserSite(Map<String,Object> model){
        return "register";
    }
}
