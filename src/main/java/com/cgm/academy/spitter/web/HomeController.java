package com.cgm.academy.spitter.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class HomeController {

    @Value("${home.welcome}")
    private String message = "Hello World!";

    @RequestMapping(value ={"/","/home"}, method = RequestMethod.GET)
    public String homeSite(Map<String,Object> model){
        model.put("message", this.message);
        return "home";
    }

}
