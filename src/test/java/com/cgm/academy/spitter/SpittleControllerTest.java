package com.cgm.academy.spitter;

import com.cgm.academy.spitter.data.SpittleRepository;
import com.cgm.academy.spitter.model.Spittle;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SpittleControllerTest {
    private String MESSAGE = "message";

    @MockBean
    private SpittleRepository spittleRepository;
    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        Spittle mockSpittle = new Spittle(MESSAGE, LocalDate.now());
        when(spittleRepository.findAll()).thenReturn(Lists.newArrayList(mockSpittle));
    }

    @Test
    public void spittlesSiteTest() throws Exception {
        mockMvc.perform(get("/spittles"))
                .andExpect(status().isOk())
                .andExpect(view().name("spittles"))
                .andExpect(content().string(containsString("Spittles")));
    }

    @Test
    public void shouldFindSpittle() throws Exception {
        mockMvc.perform(get("/spittles"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(MESSAGE)));
    }

}
